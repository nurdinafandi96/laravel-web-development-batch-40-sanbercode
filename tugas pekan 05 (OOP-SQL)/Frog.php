<?php

// $frog = new Frog("buduk");

// echo $->name; // "buduk"
// echo $frog->legs; // 4
// echo $frog->cold_blooded // no
// echo $frog->jump() ; //Hop Hop

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

require_once('Animal.php');

class Frog extends Animal
{
    public $name = "buduk";
    public $legs = 4;
    public $cold_blooded = "no";
    public $jump = "Hop Hop";
}

?>