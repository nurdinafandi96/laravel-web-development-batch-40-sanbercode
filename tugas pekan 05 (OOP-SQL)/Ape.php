<?php

// $sungokong = new Ape("kera sakti");

// echo $sungokong->name; // "kera sakti"
// echo $sungokong->legs; // 2
// echo $sungokong->cold_blooded // false
// echo $sungokong->yell() ; //Auoo

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

require_once('Animal.php');

class Ape extends Animal
{
    public $name = "kera sakti";
    public $cold_blooded = "no";
    public $yell = "Auooo";
}

?>