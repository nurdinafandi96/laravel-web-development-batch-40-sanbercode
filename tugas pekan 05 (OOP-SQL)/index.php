<?php

require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>";
echo "legs : " . $sheep->legs = 4 . "<br>";
echo "cold blooded : " . $sheep->cold_blooded = "no" . "<br><br>";

$Frog = new Frog("buduk");

echo "Name : " . $Frog->name . "<br>";
echo "legs : " . $Frog->legs . "<br>";
echo "cold blooded : " . $Frog->cold_blooded . "<br>";
echo "Jump : " . $Frog->jump . "<br><br>";

$Ape = new Ape("kera sakti");

echo "Name : " . $Ape->name . "<br>";
echo "legs : " . $Ape->legs . "<br>";
echo "cold blooded : " . $Ape->cold_blooded . "<br>";
echo "Yell : " . $Ape->yell . "<br>";

?>