@extends('layout.master')

@section('judul')
    <h4>Halaman form tambah data cast</h4>
@endsection
@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name='nama'>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <label>Umur</label>
      <input type="text" class="form-control" name='umur'>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
      <label>BIO</label>
      <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">submit</button>
</form>
@endsection