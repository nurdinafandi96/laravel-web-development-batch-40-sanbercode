@extends('layout.master')

@section('judul')
    <h4>Halaman Detail Cast</h4>
@endsection
@section('content')

<h3>{{$cast->nama}}</h3>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">kembali</a>
@endsection