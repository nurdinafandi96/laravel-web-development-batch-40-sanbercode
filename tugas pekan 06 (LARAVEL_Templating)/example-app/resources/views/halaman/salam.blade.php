<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    @extends('layout.master')

    @section('judul')
    <h3>Halaman Home</h3>
    @endsection
    
    @section('content')
    <h1>SELAMAT DATANG {{$firstName}} {{$lastName}} !</h1>
    <H2>Terima kasih telah bergabung di sanberbook. Social Media kita bersama!</H2>
    @endsection
</body>
</html>