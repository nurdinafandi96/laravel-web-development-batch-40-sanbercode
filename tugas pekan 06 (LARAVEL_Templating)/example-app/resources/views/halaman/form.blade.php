<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    @extends('layout.master')

    @section('judul')
    <h1>Halaman Biodata</h1>
    @endsection
    @section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label>First name :</label><br>
        <input type="text" name="fname"><br><br>
        <label>Last name :</label><br>
        <input type="text" name="lname"><br><br>
        <label>Gender :</label><br>
        <input type="radio" name="g">Male<br>
        <input type="radio" name="g">Female<br>
        <input type="radio" name="g">Other<br><br>
        <label>Nationality :</label><br>
        <select name="country">
            <option value="english">English</option>
            <option value="indonesia">Indonesia</option>
            <option value="arab">Arab</option>
            <option value="jepang">Japan</option>
        </select> <br> <br>
        <label>Language Spoken :</label><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Other<br><br>
        <label>Bio :</label><br>
        <textarea name="massage" rows="15" cols="30"></textarea><br><br>
        <input type="submit" value="sign up">
    </form>
    @endsection
</body>
</html>