@extends('layout.master')

@section('judul')
    <h4>Halaman tampil data film</h4>
@endsection
@section('content')
@auth
<a href="/film/create" class="btn btn-primary my-2">tambah film</a>
@endauth

<div class="row">
    @forelse ($film as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('image/' . $item->poster)}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h3>{{$item->judul}}</h3>
                    <small class="text-info">{{$item->genre->nama}}</small>
                    <p class="card-text">{{$item->tahun}}</p>
                    <p class="card-text">{{Str::limit($item->ringkasan,30)}}</p>
                    <a href="/film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Read me</a>
                    <div class="row my-2">
                        @auth
                        <div class="col">
                            <a href="/film/{{$item->id}}/edit" class="btn btn-info btn-block btn-sm">edit</a>
                        </div>
                        <div class="col">
                            <form action="film/{{$item->id}}" method="POST">
                                @csrf
                                @method('delete')
                                <input type="submit" class="btn btn-danger btn-block btn-sm" value="delete">
                            </form>
                        </div>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    @empty
        <h2>tidak ada kategori film</h2>
    @endforelse
</div>

{{-- <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($film as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/film/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Data Film Masih Kosong</td>
            </tr>
        @endforelse
    </tbody>
</table> --}}
@endsection