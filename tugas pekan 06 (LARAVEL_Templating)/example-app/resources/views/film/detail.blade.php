@extends('layout.master')

@section('judul')
    <h4>Halaman Detail Film</h4>
@endsection
@section('content')
    <img src="{{asset('image/' . $film->poster)}}" class="card-img-top" alt="...">
    <h3>{{$film->judul}}</h3>
    <p class="card-text">{{$film->tahun}}</p>
    <p class="card-text">{{$film->ringkasan}}</p>
    <hr>
    @forelse ($film->kritik as $item)
        <div class="card">
            <div class="card-header">
                Komentar dari --{{$item->User->name}}--
            </div>
            <div class="card-body">
                <h5>{{$item->point}} Point</h5>
                <p class="card-text">{{$item->content}}</p>
            </div>
        </div>
    @empty
        <h4>Belum ada komentar</h4>
    @endforelse
    <hr>
    <div>
        <form action="/kritik/{{$film->id}}" method="POST">
            @csrf
            <select name="point" class="form-control" id="">
                <option value="">--Berikan Rating--</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select><br>
            <textarea name="content" class="form-control" id="" cols="30" rows="10" placeholder="isi kritik dan saran"></textarea><br>
            <input type="submit" class="btn btn-primary btn-block btn-sm mt-2" value="Kirim Komentar"><br>
        </form>
    </div>
    <a href="/create" class="btn btn-secondary btn-block btn-sm">kembali</a>
@endsection