@extends('layout.master')

@section('judul')
    <h4>Halaman Detail Genre</h4>
@endsection
@section('content')

<h3>{{$genre->nama}}</h3>
<div class="row">
    @forelse ($genre->film as $item)
        <div class="col-4">
            <div class="col-12">
                <div class="card">
                    <img src="{{asset('image/' . $item->poster)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h3>{{$item->judul}}</h3>
                        <small class="text-info">{{$item->genre->nama}}</small>
                        <p class="card-text">{{$item->tahun}}</p>
                        <p class="card-text">{{ Str::limit($item->ringkasan, 30) }}</p>
                        <a href="/film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Read me</a>
                    </div>
                </div>
            </div>
        </div>
    @empty
        
    @endforelse
</div>

<a href="/genre" class="btn btn-secondary btn-sm">kembali</a>

@endsection