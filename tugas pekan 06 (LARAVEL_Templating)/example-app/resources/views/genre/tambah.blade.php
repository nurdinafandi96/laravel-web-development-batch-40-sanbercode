@extends('layout.master')

@section('judul')
    <h4>Halaman tambah data nama genre</h4>
@endsection
@section('content')
<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name='nama'>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">submit</button>
</form>
@endsection