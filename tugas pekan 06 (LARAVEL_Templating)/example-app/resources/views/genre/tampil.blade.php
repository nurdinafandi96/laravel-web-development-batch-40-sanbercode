@extends('layout.master')

@section('judul')
    <h4>Halaman tampil data nama genre</h4>
@endsection
@section('content')
@auth
<a href="/genre/create" class="btn btn-primary my-2">tambah genre</a>   
@endauth
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/genre/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        @auth
                        <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">   
                        @endauth
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Data Nama Genre Masih Kosong</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection