<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kritik;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function store(Request $request, $id)
    {
        $request->validate([
            'content' => 'required',
            'point' => 'required',
        ]);
        kritik::create([
            'users_id' => Auth::id(),
            'film_id' => $id,
            'content' => $request['content'],
            'point' => $request['point'],
        ]);

        return redirect('/film/'. $id);
    }
}
