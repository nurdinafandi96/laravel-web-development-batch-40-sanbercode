<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('halaman.form');
    }

    public function kirim(Request $request)
    {
        $firstName = $request['fname'];
        $lastName = $request['lname'];

        return view('halaman.salam', ['firstName' => $firstName, 'lastName' => $lastName]);
    }

    public function salam()
    {
        return view('halaman.salam');
    }

    public function submit(Request $request)
    {
        dd($request->all());
    }
}
