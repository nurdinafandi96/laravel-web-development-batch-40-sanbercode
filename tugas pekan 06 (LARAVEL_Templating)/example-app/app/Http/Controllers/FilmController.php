<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;
use App\Models\film;
use App\Models\genre;
use App\Models\kritik;
use File;


class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    public function index()
    { 
        $film = film::get();

        return view('film.tampil', ['film'=>$film]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::get();
        return view('film.create', ['genre' => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|image|mimes:jpg,png,jpeg',
        ]);

        //simpan data ke databsase
        $fileName = time().'.'.$request->poster->extension();

        $request->poster->move(public_path('image'), $fileName);
        
        $film = new Film;
        
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $fileName;
        $film->genre_id = $request->genre_id;
        
        $film->save();

        //mengarahkan ke halaman cast di tampilan view
        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);

        return view('film.detail', ['film'=>$film]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::get();

        return view('film.update', ['film' => $film, 'genre' => $genre]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'image|mimes:jpg,png,jpeg',
        ]);

        //update data berdasarkan id
        $film = Film::Find($id);
        if($request->has('poster')){
            $path = 'image/';
            File::delete($path. $film->poster);

            $fileName = time().'.'.$request->poster->extension();

            $request->poster->move(public_path('image'), $fileName);

            $film->poster = $fileName;

            $film->save();
        }
        $film->judul = $request['judul'];
        $film->tahun = $request['tahun'];
        $film->ringkasan = $request['ringkasan'];
        $film->genre_id = $request['genre_id'];
        $film->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);
 
        $film->delete();

        $path = 'image/';
        File::delete($path. $film->poster);

        return redirect('/film');
    }
}
