<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function utama()
    {
        return view('welcome');
    }

    public function table()
    {
        return view('data-table');
    }
}
