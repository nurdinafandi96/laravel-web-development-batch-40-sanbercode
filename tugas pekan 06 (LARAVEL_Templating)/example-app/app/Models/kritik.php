<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kritik extends Model
{
    use HasFactory;
    protected $table = 'kritik';
    protected $fillable = ['users_id', 'film_id', 'content', 'point'];
    public function film()
    {
        return $this->belongsTo(film::class, 'film_id');
    }
    public function User()
    {
        return $this->belongsTo(user::class, 'users_id');
    }
}
