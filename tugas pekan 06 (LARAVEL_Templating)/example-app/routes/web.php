<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\BeritaController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\KritikController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/master', function(){
//     return view('layout.master');
// });


Route::get('/', [IndexController::class, 'utama']);

Route::get('/form', [AuthController::class, 'form']);

Route::post('/kirim', [AuthController::class, 'kirim']);

Route::get('/data-table', function(){
    return view('halaman.table');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function () {
//CRUD Genre
//Create Data
//menampilkan genre 
Route::get('/genre/create', [GenreController::class, 'create']);
//menyimpan data 
Route::post('/genre', [GenreController::class, 'store']);
//Read Data
//tampil semua data di table
Route::get('/genre', [GenreController::class, 'index']);
//detail kategori berdasarkan id
Route::get('/genre/{genre_id}', [GenreController::class, 'show']);
//Update Data
//route untuk mengarah ke form edit cast berdasarkan id
Route::get('/genre/{genre_id}/edit', [GenreController::class, 'edit']);
//update data berdasarkan id
Route::put('/genre/{genre_id}', [GenreController::class, 'update']);
//Delete Data
Route::delete('/genre/{genre_id}', [GenreController::class, 'destroy']);
Auth::routes();


//CRUD Film
//Create Data
//menampilkan film 
Route::get('/film/create', [FilmController::class, 'create']);
//menyimpan data 
Route::post('/film', [FilmController::class, 'store']);
//Read Data
//tampil semua data di table
Route::get('/film', [FilmController::class, 'index']);
//detail kategori berdasarkan id
Route::get('/film/{genre_id}', [FilmController::class, 'show']);
//Update Data
//route untuk mengarah ke form edit cast berdasarkan id
Route::get('/film/{genre_id}/edit', [FilmController::class, 'edit']);
//update data berdasarkan id
Route::put('/film/{genre_id}', [FilmController::class, 'update']);
//Delete Data
Route::delete('/film/{genre_id}', [FilmController::class, 'destroy']);
Auth::routes();

//route kritik
Route::post('/kritik/{film_id}', [KritikController::class, 'store']);
});

//CRUD
//Create Data
//menampilkan form untuk membuat data pemain film baru
Route::get('/cast/create', [CastController::class, 'create']);
//menyimpan data baru ke tabel Cast
Route::post('/cast', [CastController::class, 'store']);
//Read Data
//tampil semua data di table
Route::get('/cast', [CastController::class, 'index']);
//detail kategori berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
//Update Data
//route untuk mengarah ke form edit cast berdasarkan id
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//update data berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
//Delete Data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
Auth::routes();